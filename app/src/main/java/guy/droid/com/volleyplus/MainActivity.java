package guy.droid.com.volleyplus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.daimajia.swipe.SwipeLayout;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    CustomList customList;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> friendsList;
    private ArrayList<Integer> id;
    private TextView totalClassmates;
    private SwipeLayout swipeLayout;

    private final static String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = (ListView)findViewById(R.id.list_item);

        friendsList = new ArrayList<>();
        id = new ArrayList<>();

        friendsList.add("RA");
        friendsList.add("AA");
        friendsList.add("DA");
        friendsList.add("SA");
        friendsList.add("RAZ");
        friendsList.add("AAZ");
        friendsList.add("DAZ");
        friendsList.add("SAZ");
        friendsList.add("RAAA");
        friendsList.add("AAAA");

        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);
        id.add(0);




        setListViewAdapter();
    }





    private void setSwipeViewFeatures() {
        //set show mode.
        swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        //add drag edge.(If the BottomView has 'layout_gravity' attribute, this line is unnecessary)
        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, findViewById(R.id.bottom_wrapper));

        swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                Log.i(TAG, "onClose");
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                Log.i(TAG, "on swiping");
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                Log.i(TAG, "on start open");
            }

            @Override
            public void onOpen(SwipeLayout layout) {
                Log.i(TAG, "the BottomView totally show");
            }

            @Override
            public void onStartClose(SwipeLayout layout) {
                Log.i(TAG, "the BottomView totally close");
            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
    }

    private void setListViewAdapter() {
      //  adapter = new ListViewAdapter(this, R.layout.item_listview, friendsList);
        customList = new CustomList(MainActivity.this,friendsList,id);
        listView.setAdapter(customList);

       // totalClassmates.setText("(" + friendsList.size() + ")");
    }

    public void updateAdapter() {
       // adapter.notifyDataSetChanged(); //update adapter
        customList.notifyDataSetChanged();
      //  totalClassmates.setText("(" + friendsList.size() + ")"); //update total friends in list
    }
}
