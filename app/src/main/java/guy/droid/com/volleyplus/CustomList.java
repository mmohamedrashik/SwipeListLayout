package guy.droid.com.volleyplus;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;

import java.util.ArrayList;

/**
 * Created by admin on 10/17/2016.
 */

public class CustomList extends ArrayAdapter<String> {
    private static final int ITEM_VIEW_TYPE_ITEM = 0;
    private static final int ITEM_VIEW_TYPE_SEPARATOR = 1;
    private final MainActivity context;
    private final ArrayList<String> friends;
    private final ArrayList<Integer> id;
    private SwipeLayout swipeLayout;
    private TextView name;

    public CustomList(MainActivity context, ArrayList<String> friends, ArrayList<Integer> id) {
       super(context, R.layout.item_listview,friends);
        this.context = context;
        this.id = id;
        this.friends = friends;


    }


    @Override
    public View getView(int position, View view, ViewGroup parent) {

        ViewHolder holder = null;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            // inflate UI from XML file
            view = inflater.inflate(R.layout.item_listview, parent, false);
            // get all UI view
            holder = new ViewHolder(view);

            // set tag for holder
            view.setTag(holder);
        } else {
            // if holder created, get tag from view
            holder = (ViewHolder) view.getTag();
        }

        holder.name.setTag(position);
        holder.name.setText(friends.get(position)+"  "+ holder.name.getTag());
        holder.delete.setTag(position);
        holder.delete.setOnClickListener(Delete(position,holder));
        holder.editq.setTag(position);
        holder.editq.setOnClickListener(Colored(position,holder));
        if(id.get(position) == 0)
        {
            holder.linearLayout.setBackgroundColor(Color.WHITE);
        }
        else
        {
            holder.linearLayout.setBackgroundColor(Color.GREEN);
        }



        return view;
    }


    private View.OnClickListener Delete(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // showEditDialog(position, holder);
                friends.remove(position);
                id.remove(position);
                context.updateAdapter();

                holder.swipeLayout.close();

            }
        };
    }

    private View.OnClickListener Colored(final int position, final ViewHolder holder) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                // showEditDialog(position, holder);
                id.set(position,1);
                Toast.makeText(context,""+holder.name.getTag(),Toast.LENGTH_LONG).show();

               context.updateAdapter();
                holder.swipeLayout.close();

            }
        };
    }


    private class ViewHolder {
        private TextView name;

        private SwipeLayout swipeLayout;

        LinearLayout linearLayout;

        ImageView delete;

        ImageView editq;

        public ViewHolder(View v) {
            swipeLayout = (SwipeLayout)v.findViewById(R.id.swipe_layout);
            name = (TextView) v.findViewById(R.id.name);
            delete = (ImageView)v.findViewById(R.id.delete);
            editq = (ImageView)v.findViewById(R.id.edit_query);
            linearLayout = (LinearLayout) v.findViewById(R.id.listholder);
            swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        }
    }


}
